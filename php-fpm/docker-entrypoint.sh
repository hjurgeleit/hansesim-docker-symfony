#!/bin/bash
set -e

chown www-data:www-data /var/www

exec "$@"