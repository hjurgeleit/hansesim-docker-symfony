hansesim-docker-symfony
==============

A Docker environment for creating the required services to run a developement environment for the hansesim project of the University of Applied Scienes Lübeck, Germany.
It is inspired by [Vincent Composieux's docker-symfony](https://github.com/eko/docker-symfony) Docker POC. **Kudos to eko!**

# Installation

First, clone this repository:

```bash
$ git clone git@bitbucket.org:hjurgeleit/hansesim-docker-symfony.git
```

Next, put your Symfony application into `../symfony` folder and do not forget to add `symfony.dev` in your `/etc/hosts` file.

Then, run:

```bash
$ docker-compose up
```

You are done, you can visite your Symfony application on the following URL: `http://symfony.dev`

_Note :_ you can rebuild all Docker images by running:

```bash
$ docker-compose build
```

# How it works?

Here are the `docker-compose` built images:

* `application`: This is the Symfony application code container,
* `db`: This is the MySQL database container (can be changed to postgresql or whatever in `docker-compose.yml` file),
* `php`: This is the PHP-FPM container in which the application volume is mounted,
* `nginx`: This is the Nginx webserver container in which application volume is mounted too,


# Read logs

You can access Nginx and Symfony application logs in the following directories into your host machine:

* `logs/nginx`
* `logs/symfony`